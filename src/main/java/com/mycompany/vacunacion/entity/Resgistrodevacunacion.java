/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vacunacion.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author andre
 */
@Entity
@Table(name = "resgistrodevacunacion")
@NamedQueries({
    @NamedQuery(name = "Resgistrodevacunacion.findAll", query = "SELECT r FROM Resgistrodevacunacion r"),
    @NamedQuery(name = "Resgistrodevacunacion.findByNombre", query = "SELECT r FROM Resgistrodevacunacion r WHERE r.nombre = :nombre"),
    @NamedQuery(name = "Resgistrodevacunacion.findByRut", query = "SELECT r FROM Resgistrodevacunacion r WHERE r.rut = :rut"),
    @NamedQuery(name = "Resgistrodevacunacion.findByTelefono", query = "SELECT r FROM Resgistrodevacunacion r WHERE r.telefono = :telefono"),
    @NamedQuery(name = "Resgistrodevacunacion.findByAlergia", query = "SELECT r FROM Resgistrodevacunacion r WHERE r.alergia = :alergia")})
public class Resgistrodevacunacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Lob
    @Column(name = "direccion ")
    private Object direccion;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 2147483647)
    @Column(name = "alergia")
    private String alergia;

    public Resgistrodevacunacion() {
    }

    public Resgistrodevacunacion(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public Object getDireccion() {
        return direccion;
    }

    public void setDireccion(Object direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getAlergia() {
        return alergia;
    }

    public void setAlergia(String alergia) {
        this.alergia = alergia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resgistrodevacunacion)) {
            return false;
        }
        Resgistrodevacunacion other = (Resgistrodevacunacion) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.vacunacion.entity.Resgistrodevacunacion[ rut=" + rut + " ]";
    }
    
}
