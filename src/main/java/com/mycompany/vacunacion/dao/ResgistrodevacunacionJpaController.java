/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vacunacion.dao;

import com.mycompany.vacunacion.dao.exceptions.NonexistentEntityException;
import com.mycompany.vacunacion.dao.exceptions.PreexistingEntityException;
import com.mycompany.vacunacion.entity.Resgistrodevacunacion;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author andre
 */
public class ResgistrodevacunacionJpaController implements Serializable {

    public ResgistrodevacunacionJpaController() {
       
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Resgistrodevacunacion resgistrodevacunacion) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(resgistrodevacunacion);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findResgistrodevacunacion(resgistrodevacunacion.getRut()) != null) {
                throw new PreexistingEntityException("Resgistrodevacunacion " + resgistrodevacunacion + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Resgistrodevacunacion resgistrodevacunacion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            resgistrodevacunacion = em.merge(resgistrodevacunacion);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = resgistrodevacunacion.getRut();
                if (findResgistrodevacunacion(id) == null) {
                    throw new NonexistentEntityException("The resgistrodevacunacion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Resgistrodevacunacion resgistrodevacunacion;
            try {
                resgistrodevacunacion = em.getReference(Resgistrodevacunacion.class, id);
                resgistrodevacunacion.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The resgistrodevacunacion with id " + id + " no longer exists.", enfe);
            }
            em.remove(resgistrodevacunacion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Resgistrodevacunacion> findResgistrodevacunacionEntities() {
        return findResgistrodevacunacionEntities(true, -1, -1);
    }

    public List<Resgistrodevacunacion> findResgistrodevacunacionEntities(int maxResults, int firstResult) {
        return findResgistrodevacunacionEntities(false, maxResults, firstResult);
    }

    private List<Resgistrodevacunacion> findResgistrodevacunacionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Resgistrodevacunacion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Resgistrodevacunacion findResgistrodevacunacion(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Resgistrodevacunacion.class, id);
        } finally {
            em.close();
        }
    }

    public int getResgistrodevacunacionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Resgistrodevacunacion> rt = cq.from(Resgistrodevacunacion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
