/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.vacunacion.dao.ResgistrodevacunacionJpaController;
import com.mycompany.vacunacion.dao.exceptions.NonexistentEntityException;
import com.mycompany.vacunacion.entity.Resgistrodevacunacion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andre
 */
@WebServlet(name = "RegistroController", urlPatterns = {"/RegistroController"})
public class RegistroController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegistroController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegistroController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String accion=request.getParameter("accion");
        
        if (accion.equals("ingresar")){
            
        
        
        try {
            String nombre=request.getParameter("nombre");
            String rut=request.getParameter("rut");
            String direccion=request.getParameter("direccion");
            String telefono=request.getParameter("telefono");
            String alergia=request.getParameter("alergia");
            
            Resgistrodevacunacion resgistrodevacunacion=new Resgistrodevacunacion();
            resgistrodevacunacion.setNombre(nombre);
            resgistrodevacunacion.setRut(rut);
            resgistrodevacunacion.setDireccion(direccion);
            resgistrodevacunacion.setTelefono(telefono);
            resgistrodevacunacion.setAlergia(alergia);
            
            ResgistrodevacunacionJpaController dao=new ResgistrodevacunacionJpaController();
            
            dao.create(resgistrodevacunacion);
            
            
        } catch (Exception ex) {
            Logger.getLogger(RegistroController.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        if (accion.equals("lista")){
            
            ResgistrodevacunacionJpaController dao=new ResgistrodevacunacionJpaController();
           List< Resgistrodevacunacion> lista= dao.findResgistrodevacunacionEntities();
            request.setAttribute("listavacunados", lista);
            request.getRequestDispatcher("listado.jsp").forward(request, response);
        }
        if(accion.equals("eliminar")){
            try {
                String rut= request.getParameter("seleccion");
                ResgistrodevacunacionJpaController dao=new ResgistrodevacunacionJpaController();
                dao.destroy(rut);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(RegistroController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        if(accion.equals("consultar")){
            String nombre= request.getParameter("seleccion");
             ResgistrodevacunacionJpaController dao=new ResgistrodevacunacionJpaController();
           Resgistrodevacunacion registro=  dao.findResgistrodevacunacion(nombre);
            request.setAttribute("solicitud", registro);
           request.getRequestDispatcher("consulta.jps").forward(request, response);
        }
        
        processRequest(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
